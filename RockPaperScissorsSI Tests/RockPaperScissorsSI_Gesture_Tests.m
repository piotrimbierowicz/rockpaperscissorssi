//
//  RockPaperScissorsSI_Gesture_Tests.m
//  RockPaperScissorsSI
//
//  Created by Piotr Imbierowicz on 14.06.2014.
//  Copyright (c) 2014 Piotr Imbierowicz. All rights reserved.
//

#import <XCTest/XCTest.h>
#include "rock_paper_scissors_si.h"

@interface RockPaperScissorsSI_Gesture_Tests : XCTestCase

@end

@implementation RockPaperScissorsSI_Gesture_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    RockPaperScissorsSI rps_si = new RockPaperScissorsSI();
}

@end
