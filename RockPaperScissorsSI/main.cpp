//
//  main.cpp
//  RockPaperScissorsSI
//
//  Created by Piotr Imbierowicz on 14.06.2014.
//  Copyright (c) 2014 Piotr Imbierowicz. All rights reserved.
//

#include <iostream>
#include "rock_paper_scissors_si.h"

int main(int argc, const char * argv[])
{
    RockPaperScissorsSI *rps_si = new RockPaperScissorsSI();
    int game_mode = 0;
    printf("1 - kamień, 2 - papier, 3 - nożyce, 5 - stan gry, 9 - wyjście\n");
    while(game_mode != 9) {
        scanf("%d", &game_mode);
        switch (game_mode) {
            case 1:
                rps_si->add_selection(RockPaperScissorsSI::Rock, rps_si->get_selection());
                printf("%s\n", rps_si->last_game_to_string().c_str());
                break;
            case 2:
                rps_si->add_selection(RockPaperScissorsSI::Paper, rps_si->get_selection());
                printf("%s\n", rps_si->last_game_to_string().c_str());
                break;
            case 3:
                rps_si->add_selection(RockPaperScissorsSI::Scissors, rps_si->get_selection());
                printf("%s\n", rps_si->last_game_to_string().c_str());
                break;
            case 5:
                printf("%s\n", rps_si->to_string().c_str());
                break;
            default:
                break;
        }
        rps_si->find_patterns();
    }
    
    return 0;
}

